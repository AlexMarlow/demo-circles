/**
 * @module main app module
 */

import UI from './js/ui/UI.js';
import CanvasEditor from './js/canvas/CanvasEditor.js';
import Circle from './js/canvas/Circle.js';

const CLOCKWISE_ROTATION = '+';
const COUNTERCLOCKWISE_ROTATION = '1';
const mainApp = {};

mainApp.init = function() {

    const self = mainApp;

    let btn_Rotate = new UI.Button( 'Change rotation', changeRotation_handler );
    let btn_ChangeColor = new UI.Button( 'Change color', changeColor_handler );

    self.editor = new CanvasEditor();
    // create UI objects
    self.body = new UI.Element( document.body );
    self.body.add( self.editor.canvas );
    self.body.add( btn_ChangeColor );
    self.body.add( btn_Rotate );

    let canvas = self.editor.canvas.dom;
    canvas.width = 480;
    canvas.height = 320;

    let orbit = { centerX: canvas.width / 2, centerY: canvas.height / 2, radius: 60, angle:0 };
    let speed = 0.01;
    let rotation = CLOCKWISE_ROTATION;

    // create three circles
    let firstArc = new Circle( 0, 0, 10 );
    firstArc.angle = 0;
    firstArc.color = getRandomColor();

    let secondArc = new Circle( 0, 0, 10 );
    secondArc.angle = ( Math.PI * 2 ) / 3;
    secondArc.color = getRandomColor();

    let thirdArc = new Circle( 0, 0, 10 );
    thirdArc.angle = ( Math.PI * 4 ) / 3;
    thirdArc.color = getRandomColor();

    self.editor.addObject( firstArc, secondArc, thirdArc );
    animateCanvas();

    //TODO: move to method of CanvasEditor?
    function animateCanvas() {
        firstArc.angle = getAngle( firstArc.angle, speed, rotation );
        firstArc.setPosition( getPosX( firstArc.angle ), getPosY( firstArc.angle ) );

        secondArc.angle = getAngle( secondArc.angle, speed, rotation );
        secondArc.setPosition( getPosX( secondArc.angle ), getPosY( secondArc.angle ) );

        thirdArc.angle = getAngle( thirdArc.angle, speed, rotation );
        thirdArc.setPosition( getPosX( thirdArc.angle ), getPosY( thirdArc.angle ) );

        self.editor.render();
        requestAnimationFrame( animateCanvas );
    }
    // buttons handlers
    function changeRotation_handler() {
        if ( rotation === CLOCKWISE_ROTATION ) rotation = COUNTERCLOCKWISE_ROTATION;
        else rotation = CLOCKWISE_ROTATION;
    }
    function changeColor_handler() {
        firstArc.color = getRandomColor();
        secondArc.color = getRandomColor();
        thirdArc.color = getRandomColor();
    }
    // TODO: move to external module (Utils?)
    function getPosX( angle ) {
        return orbit.centerX + Math.cos( angle ) * orbit.radius;
    }
    function getPosY( angle ) {
        return orbit.centerY + Math.sin( angle ) * orbit.radius;
    }
    function getAngle( angle, value, sign ) {
        switch( sign ) {
            case CLOCKWISE_ROTATION:
                return angle += value;
            case COUNTERCLOCKWISE_ROTATION:
                return angle -= value;
        }
    }
    function getRandomColor() {
        const letters = '0123456789ABCDEF';
        let color = '#';
        for ( let i = 0; i < 6; i++ ) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
};

window.onload = mainApp.init;

export default mainApp;