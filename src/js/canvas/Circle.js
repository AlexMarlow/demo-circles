/**
 * @module simple canvas circle
 */

import CanvasObject from './CanvasObject.js';

export default class Circle extends CanvasObject {
    /**
     * @constructor
     * @param {Number} x - x position
     * @param {Number} y - y position
     * @param {Number} radius
     */
    constructor( x, y, radius ) {
        super();
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.color = '#000000';
    }
    draw() {
        let context = this.parentContext2D;
        context.fillStyle = this.color;
        context.beginPath();
        context.arc( this.x, this.y, this.radius, 0, 2 * Math.PI, false);
        context.fill();
    }
    setPosition( x, y ) {
        this.x = x;
        this.y = y;
    }

}