/**
 * @module base class for all Canvas objects
 */

export default class CanvasObject extends Path2D {

    constructor(){
        super();
        this.parentContext2D = null;
    }

}