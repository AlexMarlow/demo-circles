/**
 * @module canvas (editor?)
 */

import UI from '../ui/UI.js';

export default class CanvasEditor {

    constructor() {
        this.canvas = new UI.Canvas();
        this.objects = [];
        this.context2D = this.canvas.dom.getContext( '2d' );
    }

    render() {
        this.clear();
        this.objects.forEach( object => {
            object.draw();
        });
    }

    clear() {
        this.context2D.clearRect(0, 0, this.canvas.dom.width, this.canvas.dom.height);
    }

    addObject( object ) {
        const self = this;
        if ( arguments.length > 1 ) {
            for ( let i = 0; i < arguments.length; i++ ) {
                self.addObject( arguments[i] );
            }
            return this;
        }
        //TODO: redo through events
        object.parentContext2D = this.context2D;
        this.objects.push( object );
    }
}