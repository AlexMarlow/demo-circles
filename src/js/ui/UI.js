/**
 * @module simple lib of UI elements
 */

const UI = {};

UI.Element = class Element {
    /**
     * Base Class for all UI elements
     * @constructor
     * @param {Object} dom - DOM object
     */
    constructor( dom ) {
        this.dom = dom;
    }

    onClick( callback ) {
        this.dom.addEventListener( 'click', callback, false );
        return this;
    }

    add( element ) {
        this.dom.appendChild( element.dom );
    }

};

UI.Canvas = class Canvas extends UI.Element {

    constructor(){
        let dom = document.createElement( 'canvas' );
        super( dom );
    }

};

UI.Button = class Button extends UI.Element {
    /**
     * @constructor
     * @param {String} textValue - button text
     * @param {Function} onClick
     */
    constructor( textValue, onClick ) {
        let dom = document.createElement( 'button' );
        super( dom );
        this.dom.textContent = textValue;
        if ( onClick != null ) this.onClick( onClick );
    }

};

export default UI;